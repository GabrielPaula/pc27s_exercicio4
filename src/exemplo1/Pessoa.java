package exemplo1;

/**
 * Created by gabri on 05/10/2017.
 */
public class Pessoa implements Runnable {

    Conta contaCompartilhada;

    String nome;

    public Pessoa(Conta contaCompartilhada, String nome) {
        this.contaCompartilhada = contaCompartilhada;
        this.nome = nome;
    }

    @Override
    public void run() {

        contaCompartilhada.depositar(50.00, nome);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        contaCompartilhada.sacar(100.00, nome);

    }
}
