package exemplo1;

/**
 * Created by gabri on 05/10/2017.
 */
public class Conta{

    private Double valor = 0.0;

    /**
     * Método para saque. A operação de saque será sequencial
     * @param valor
     * @return
     */
    public synchronized Double sacar(Double valor, String nome){
        if(valor > this.valor){
            System.out.println( nome + " não conseguiu sacar, valor maior do q tem na conta!");
            return 0.0;
        } else {
            System.out.println(nome + " Sacou " + valor);
            this.valor = this.valor - valor;
            return this.valor;
        }
    }

    /**
     * Método para depóito.
     * @param valor
     */
    public void  depositar(Double valor, String nome){
        System.out.println(nome + " depositou " + valor);
        this.valor += valor;
    }

}
